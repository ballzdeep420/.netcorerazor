# Movie Rating Web Application

Simple Web Application that reads and writes to a database information on movies.

## Getting Started

To run the project simply clone the repository and open it in Visual Studio Environment, Build it and then Run the project.
When the project is Running, a browser window will apear saying welcome.
By clicking the top left element on the navigation bar you will access movies the have been seeded in the library(database) when the application was run.
There is a filter available to see only certen genres and a surch bar for surching a specific movie by title.
By clicking the element under Index(Create New), you can add a new movie to the library(database).
Afterwards you can Remove, Edit or view Details on each movie.

### Prerequisites

.Net Core 3.1

## Authors

* **Kristiyan Georgiev** - *Initial work*

