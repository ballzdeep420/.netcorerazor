﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MoviesAplication_v2.Data;
using MoviesAplication_v2.Models;

namespace MoviesAplication_v2.Pages.Movies
{
    public class DetailsModel : PageModel
    {
        private readonly MoviesAplication_v2.Data.MoviesAplication_v2Context _context;

        public DetailsModel(MoviesAplication_v2.Data.MoviesAplication_v2Context context)
        {
            _context = context;
        }

        public Movie Movie { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Movie = await _context.Movie.FirstOrDefaultAsync(m => m.ID == id);

            if (Movie == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
