﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using MoviesAplication_v2.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAplication_v2.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MoviesAplication_v2Context(
                serviceProvider.GetRequiredService<
                    DbContextOptions<MoviesAplication_v2Context>>()))
            {
                //Look for movies
                if (context.Movie.Any())
                {
                    return; //DB has been seeded
                }

                context.Movie.AddRange(
                    new Movie
                    {
                        Title = "Chocho",
                        ReleaseDate = DateTime.Today,
                        Genre = "Comedy",
                        Price = 399.99M,
                        Rating = "Reee"
                    },

                    new Movie
                    {
                        Title = "When Harry? Met Sally? What?",
                        ReleaseDate = DateTime.Parse("1989-2-12"),
                        Genre = "Romantic Comedy",
                        Price = 7.99M,
                        Rating = "Bad"
                    },

                    new Movie
                    {
                        Title = "Ghostbusters ",
                        ReleaseDate = DateTime.Parse("1984-3-13"),
                        Genre = "Comedy",
                        Price = 8.99M,
                        Rating = "Does the job"
                    },

                    new Movie
                    {
                        Title = "Ghostbusters 2",
                        ReleaseDate = DateTime.Parse("1986-2-23"),
                        Genre = "Comedy",
                        Price = 9.99M,
                        Rating = "Does the job"
                    },

                    new Movie
                    {
                        Title = "Rio Bravo",
                        ReleaseDate = DateTime.Parse("1959-4-15"),
                        Genre = "Western",
                        Price = 3.99M,
                        Rating = "NA"
                    },

                    new Movie
                    {
                        Title = "Doctor Who",
                        ReleaseDate = DateTime.Parse("1922-5-21"),
                        Genre = "Fentasy",
                        Price = 15.99M,
                        Rating = "Gud"
                    }
                    );
                context.SaveChanges();
            }
        }
    }
}
