﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MoviesAplication_v2.Models;

namespace MoviesAplication_v2.Data
{
    public class MoviesAplication_v2Context : DbContext
    {
        public MoviesAplication_v2Context (DbContextOptions<MoviesAplication_v2Context> options)
            : base(options)
        {
        }

        public DbSet<MoviesAplication_v2.Models.Movie> Movie { get; set; }
    }
}
